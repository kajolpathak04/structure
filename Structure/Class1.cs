﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Structure
{
    struct Product
    {
      public  int p_id;
      public  string p_name; 
     public   float p_price;  
       public Product(int p_id,string p_name,float p_price)
        {
            this.p_id= p_id;
            this.p_name = p_name;
            this.p_price = p_price;
        }
    }
}
