﻿// See https://aka.ms/new-console-template for more information
using Structure;


Customer obj1 = new Customer("kajol", 101, "kajol@");
Product obj2 = new Product(104, "TV", 784);
//Console.WriteLine($" detail of customer name : {obj1.c_name} \n Product Name : {p_name} \n Product Price: {p_price}");

Console.WriteLine($"detail of customer name: { obj1.c_name} \t id: { obj1.c_id} \t email: { obj1.c_email}");
Console.WriteLine($"detail of product name:{ obj2.p_id}\t Name:  { obj2.p_name} \t Price: { obj2.p_price}");



//Array
//declaration of single dimension 
string[] name=new string[] {"kajol","neha","priya","sumit"};
//iteration by using for loop
Console.WriteLine("iteration by using a for loop");
for(int i = 0; i < name.Length; i++)
{
    Console.WriteLine(name[i]);
}